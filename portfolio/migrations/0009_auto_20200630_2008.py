# Generated by Django 3.0.7 on 2020-06-30 18:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0008_meta'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='meta',
            options={'verbose_name_plural': 'Meta'},
        ),
    ]
