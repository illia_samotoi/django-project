from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    category_name = models.CharField(max_length=50)

    def __str__(self):
        return self.category_name

    class Meta:
        verbose_name_plural = "Categories"


class Tag(models.Model):
    tag_name = models.CharField(max_length=20)

    def __str__(self):
        return self.tag_name


class Post(models.Model):
    title = models.CharField(max_length=150)
    post_date = models.DateTimeField(auto_now_add=True)  # Дата добавляется как только был добавлен пост
    text = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    tag = models.ForeignKey(Tag, on_delete=models.PROTECT, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='uploads/%Y/%m/%d/', blank=True)

    def __str__(self):
        return self.title


class Quote(models.Model):
    text = models.TextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    author_image = models.ImageField(upload_to='uploads/%Y/%m/%d/', blank=True)

    def __str__(self):
        return self.author


class Portfolio(models.Model):
    title = models.CharField(max_length=150)
    post_date = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    image = models.ImageField(upload_to='uploads/%Y/%m/%d/', blank=True)

    def __str__(self):
        return self.title


class FeedbackForm(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField()
    subject = models.CharField(max_length=400)
    message = models.TextField()
    date_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name}, {self.subject}"


class Skills(models.Model):
    title = models.CharField(max_length=31)
    value = models.IntegerField()

    def __str__(self):
        return self.title


class Meta(models.Model):
    key = models.CharField(max_length=32, unique=True)
    value = models.TextField()

    def __str__(self):
        return self.key

    class Meta:
        verbose_name_plural = 'Meta'
