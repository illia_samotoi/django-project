from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.views.generic import DetailView, FormView
from django.urls import reverse_lazy

from portfolio.models import Post, Portfolio, Quote, FeedbackForm, Skills, Meta
from portfolio.forms import ContactFormHTML
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login


def homepage(request):
    posts = Post.objects.all()
    quotes = Quote.objects.all()
    portfolios = Portfolio.objects.all()
    contact_form = ContactFormHTML()
    skills = Skills.objects.all()

    if request.method == "POST":
        contact_form = ContactFormHTML(request.POST)

        if contact_form.is_valid():
            FeedbackForm(
                name=contact_form.cleaned_data['name'],
                email=contact_form.cleaned_data['email'],
                subject=contact_form.cleaned_data['subject'],
                message=contact_form.cleaned_data['message'],
            ).save()

    context = {
        'full_name': "Illia Samotoi",
        'job_list': ["Junior Python Developer", "Photonics and Fiber optics scientist"],
        'contacts': {
            "phone": '+38(097)0444972',
            "email": 'illia.v.samotoi@gmail.com'
        },
        'profile': "Junior Python Developer",
        'posts': posts,
        'quotes': quotes,
        'portfolios': portfolios,
        "contact_form": contact_form,
        "skills": skills,
    }
    return render(request, 'portfolio/index.html', context)


def print_context(request):
    context = {
        'friends': ["Kate", "Daria", "Sergiy", "Pavel", "Leonid", 'Illia']
    }
    return render(request, 'portfolio/context.html', context)


# def login_page(request):
#     form = AuthenticationForm()
#
#     if request.method == "POST":
#         form = AuthenticationForm(data=request.POST)
#
#         if form.is_valid():
#             user = form.get_user()
#             login(request, user)
#
#     context = {
#         'form': form,
#     }
#     return render(request, 'portfolio/login.html', context)


class Login(FormView):
    form_class = AuthenticationForm
    template_name = 'portfolio/login.html'
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user=user)
        return super().form_valid(form)


# def register_page(request):
#     form = UserCreationForm()
#
#     if request.method == 'POST':
#         form = UserCreationForm(request.POST)
#
#         if form.is_valid():
#             form.save()
#             return redirect("login")
#
#     context = {
#         'form': form,
#     }
#     return render(request, 'portfolio/register.html', context)


class Register(FormView):
    form_class = UserCreationForm
    template_name = 'portfolio/register.html'
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

# def blog_single(request, pk):
#     post = get_object_or_404(Post, pk=pk)
#     # post = Post.objects.get(pk=pk)
#
#     context = {
#         'post': post,
#     }
#     return render(request, 'portfolio/blog-single.html', context)


class BlogSingle(DetailView):
    template_name = 'portfolio/blog-single.html'
    model = Post
