from django.urls import path

from .views import homepage, print_context, Login, Register, BlogSingle

from django.views.generic import DetailView
from .models import Post

urlpatterns = [
    path('', homepage),
    path('context/', print_context),
    path('login/', Login.as_view(), name="login"),
    path('register/', Register.as_view(), name='register'),
    path('blog/<int:pk>', BlogSingle.as_view(), name='blog-single'),
]

# urlpatterns = [
#     path('', homepage),
#     path('context/', print_context),
#     path('login/', login_page, name="login"),
#     path('register/', register_page, name='register'),
#     path('blog/<int:pk>',
#     DetailView.as_view(template_name='portfolio/blog-single.html',
#     model=Post), name='blog-single'),
# ]
